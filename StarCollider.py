
from pygame.locals import *
import math #Оказывается, в pyGame был math...
from Constants import *
from Entities.Entity import *
from Utilities.Distance import *


# Сделал его Энтити просто для упрощения, вообще - это не совсем хорошо.
class StarCollider(Entity):
    # Высчитываем координаты концов относительно центра
    def _calculate_endings(self, radius, count):
        endings = []
        inc_rad = math.pi*2 / count
        current_rad = 0
        while current_rad < math.pi*2:
            endings.append((math.cos(current_rad)*radius, math.sin(current_rad)*radius))
            self.collision.append(STAR_COLLIDER_COLLIDE_NOTHING)  # небольшой костыль todo
            current_rad += inc_rad
        return endings

    def __init__(self, radius, count, owner_entity):
        super(StarCollider, self).__init__(0, radius, color, 0)
        self.radius = radius
        self.count = count
        self.collision = []  # показывает, с чем происходят коллизии для текущего конца
        self.endings = self._calculate_endings(radius, count)   # координаты относительно центра
        self.owner = owner_entity
        self.center = self.owner.center

    def update(self, dt=1, possible_rect=(0, 0, 0, 0)):
        self.center = self.owner.center

    def draw(self, surface):
        if self.owner.isAlive:
            for i in range(self.count):
                new_color = COLOR_BLACK
                if self.collision[i] == STAR_COLLIDER_COLLIDE_ENEMY:
                    new_color = COLOR_ENEMY
                if self.collision[i] == STAR_COLLIDER_COLLIDE_FOOD:
                    new_color = COLOR_FOOD
                if self.collision[i] == STAR_COLLIDER_COLLIDE_WALL:
                    new_color = COLOR_WALL
                (end_x, end_y) = self.endings[i]
                pygame.draw.line(surface, new_color,
                                 list(map(int, self.center)),
                                 [self.center[0]+end_x, self.center[1]+end_y])

    # Вот эту штуку можно долго оптимизировать ToDo
    def find_collision(self, enemies_list, food_list):
        # Грубо отсечем тех чертей, которые находятся далеко
        close_enemies = [x for x in enemies_list if distance.dist(self.center, x.center) < self.radius + x.radius]
        close_food = [x for x in food_list if distance.dist(self.center, x.center) < self.radius + x.radius]
        for i in range(self.count):     # перебираем концы
            self.collision[i] = STAR_COLLIDER_COLLIDE_NOTHING
            end = self.endings[i]
            for enemy in close_enemies:
                for food in close_food:
                    if distance.point_to_line_dist(food.center, (self.center[0],
                                                                 self.center[1],
                                                                 self.center[0] + end[0],
                                                                 self.center[1] + end[1])) < food.radius \
                            and distance.dist(food.center, (self.center[0] + end[0],
                                                            self.center[1] + end[1])) < self.radius:
                        self.collision[i] = STAR_COLLIDER_COLLIDE_FOOD
                        break  # не проверяем других поцанов т.к уже определились (Todo Сделать пропуск проверки на еду)
                if distance.point_to_line_dist(enemy.center, (self.center[0],
                                                              self.center[1],
                                                              self.center[0] + end[0],
                                                              self.center[1] + end[1])) < enemy.radius\
                        and distance.dist(enemy.center, (self.center[0] + end[0],
                                                         self.center[1] + end[1])) < self.radius: # Костыль для того, чтобы не пришлось считать расстояние до отрезка :)  TODO
                    self.collision[i] = STAR_COLLIDER_COLLIDE_ENEMY
                    break  # не проверяем других поцанов т.к уже определились (Todo Сделать пропуск проверки на еду)



