from Constants import *
from Entities.Entity import *


class Player(Entity):
    def __init__(self, center, radius=RADIUS_PLAYER, color=COLOR_PLAYER, speed=[0, 0]):
        super(Player, self).__init__(center, radius, color, speed)
        self._score = 0

    def add_score(self, count=1):
        self._score += 1

    def get_score(self):
        return self._score

