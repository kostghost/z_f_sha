import pygame
from pygame.locals import *
from Constants import *


import Utilities.Distance as distance


class Entity:
    def __init__(self, center, radius, color, speed):
        self.center = center
        self.radius = radius
        self.color = color
        self.speed = speed
        self.maxSpeed = ENTITY_MAX_SPEED
        self.acceleration = ENTITY_ACCELERATION
        self.isAlive = True

    # https://www.pygame.org/docs/ref/draw.html
    def draw(self, surface):
        if self.isAlive:
            pygame.draw.circle(surface, self.color, list(map(int, self.center)), self.radius, 0)

    def update(self, dt=1, possible_rect=(0, 0, 0, 0)):
        if self.isAlive:
            old_center = (self.center[0], self.center[1])
            self.center[0] += self.speed[0] * dt
            self.center[1] += self.speed[1] * dt

            if (self.center[0] < possible_rect[0]) | (self.center[0] > possible_rect[2]):
                self.center[0] = old_center[0]
                self.speed[0] = -self.speed[0]
            if (self.center[1] < possible_rect[1]) | (self.center[1] > possible_rect[3]):
                self.center[1] = old_center[1]
                self.speed[1] = -self.speed[1]

    # TODO Учесть нормализацию
    # Устанавливает новую скорость только в том случае, если она меньше максимальной по каждой из координат
    def set_speed(self, new_speed):
        if abs(new_speed[0]) <= abs(self.maxSpeed):
            self.speed[0] = new_speed[0]
        if abs(new_speed[1]) <= abs(self.maxSpeed):
            self.speed[1] = new_speed[1]

    # TODO Учесть нормализацию
    # Не придумал, как назвать :) изменяет ускорение в направлении (x, y)
    def go(self, direction=(0, 0)):
        self.set_speed((self.speed[0] + direction[0] * self.acceleration,
                        self.speed[1] + direction[1] * self.acceleration))

    # И никакого полиморфизма :!
    # Нет столкновения, если энтити мертв :)
    def is_collide(self, entity_b):
        if self.isAlive & entity_b.isAlive:
            if distance.dist(self.center, entity_b.center) <= self.radius + entity_b.radius:
                return True
        return False

    # Отвечает за смерть энтити
    def die(self):
        self.isAlive = False


