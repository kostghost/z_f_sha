from Constants import *
from Entities.Entity import *


class Enemy(Entity):
    def __init__(self, center, radius=RADIUS_ENEMY, color=COLOR_ENEMY, speed=[0, 0]):
        super(Enemy, self).__init__(center, radius, color, speed)
        self.maxSpeed = ENEMY_MAX_SPEED
