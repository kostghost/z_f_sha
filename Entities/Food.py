from Constants import *
from Entities.Entity import *


class Food(Entity):
    def __init__(self, center, radius=RADIUS_FOOD, color=COLOR_FOOD, speed=[0, 0]):
        super(Food, self).__init__(center, radius, color, speed)
