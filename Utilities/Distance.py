import math


def dist(point_a, point_b):
    return math.sqrt((point_b[0]-point_a[0])**2 + (point_b[1]-point_a[1])**2)


def point_to_line_dist(point_a, segment):
    # from sof https://stackoverflow.com/questions/39840030/distance-between-point-and-a-line-from-two-points работает долго
    # метод с https://stackoverflow.com/questions/27461634/calculate-distance-between-a-point-and-a-line-segment-in-latitude-and-longitude
    x0, y0 = point_a
    x1, y1, x2, y2 = segment
    nom = abs((y2 - y1) * x0 - (x2 - x1) * y0 + x2 * y1 - y2 * x1)
    denom = ((y2 - y1) ** 2 + (x2 - x1) ** 2) ** 0.5
    result = nom / denom
    # Зная, что первая координата - всегда центр, посчитаем true расстояние
    return result
