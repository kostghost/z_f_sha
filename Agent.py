import DaGame
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense
import numpy as np
import time
from numpy.random import randint

np.random.seed(42)
population_size = 10
n_epochs = 15

num_of_enemies = 18
num_of_food = 80

# для дескретизирования
min_val = -1    # == 0
max_val = 1     # == 63
bits_count = 9
sampling_count = pow(2, bits_count)
sampling_inc = (max_val - min_val) / sampling_count  # == 0.03125
format_string = "%0{}d".format(bits_count)


# 6.72 -> 101000010100010
def fen_to_gen(val):
    # import math
    return int(round((val - min_val) / sampling_inc))


# 101000010100010 -> 6.72
def gen_to_fen(int_val):
    return min_val + int_val * sampling_inc


fen_to_gen = np.vectorize(fen_to_gen)
gen_to_fen = np.vectorize(gen_to_fen)


def make_hybrids(parents):
    """parents: list of 2 parents. Each parent is a 2-d matrix of same shape."""
    from numpy.random import randint

    [x, y] = parents
    old_shape = x.shape
    separator = randint(1, x.size - 1)
    x, y = x.ravel(), y.ravel()
    # x, y = fen_to_gen(x), fen_to_gen(y)

    z, w = x.copy(), y.copy()
    z[:separator + 1] = x[:separator + 1]
    z[separator + 1:] = y[separator + 1:]
    w[separator + 1:] = x[separator + 1:]
    w[:separator + 1] = y[:separator + 1]
    zbin = format_string % int(bin(fen_to_gen(z[separator]))[2:])
    wbin = format_string % int(bin(fen_to_gen(w[separator]))[2:])
    bin_separator = randint(0, bits_count)
    new_z = int(zbin[:bin_separator+1] + wbin[bin_separator:], base=2)
    new_w = int(wbin[:bin_separator+1] + zbin[bin_separator:], base=2)
    z[separator] = new_z
    w[separator] = new_w

    # z, w = gen_to_fen(z), gen_to_fen(w)
    return [z.reshape(old_shape), w.reshape(old_shape)]


def score(lifetime, food_gathered):
    return lifetime * 0.5 + food_gathered * 0.6


def softmax(x):
    exps = np.exp(x)
    return exps / np.sum(exps)

# создание нейронок тут
def create_population(n_population_size):
    res = []
    for i in range(n_population_size):
        aoann = Sequential()
        # aoann.add(Dense(32, input_dim=shape, kernel_initializer="he_normal", activation="relu"))
        aoann.add(Dense(2, input_dim=shape, kernel_initializer="he_normal", activation='tanh'))
        new_weights = gen_to_fen(randint(0, sampling_count, size=aoann.get_weights()[0].shape))
#        print(new_weights)
        aoann.set_weights([new_weights, aoann.get_weights()[1]])
        res.append(aoann)
    return res


def one_hot_encoder(lst, count_of_different):
    return to_categorical(lst, num_classes=count_of_different)


def estimate_scores(models, epoch):
    scores = []
    for model in models:
        scores.append(make_test(model, epoch))
    return np.array(scores)


def make_test(nncontroller, epoch):
    if  epoch == 0 or epoch == n_epochs-1:
        #game = DaGame.Game(is_graphic=True, fixed_fps=150, seed=120, debug=False)
        game = DaGame.Game(is_graphic=True, fixed_fps=300, seed=120, debug=False)
    else:
        game = DaGame.Game(is_graphic=False, fixed_fps=1000000, seed=120, debug=False)
    game.set_epoch(epoch)
    game.create_entities(n_enemies=num_of_enemies, n_food=num_of_food)
    test = [True]
    food_gathered = 0
    count_of_ticks = 0
    while test[0]: # проверка на isAlive
        count_of_ticks += 1
        game.make_tick()
        test = game.get_environment_info()
        is_alive, food_gathered, speed_x, speed_y, star = test

        encoded = one_hot_encoder(star, DaGame.STAR_COLLIDER_COUNT_OF_CLASSES)
        # flat = [item for sublist in encoded for item in sublist]
        flat = np.append(encoded, [speed_x, speed_y])

        pred = nncontroller.predict(flat.reshape((1,) + (shape,)))

        game.control(0, (pred[0][0], pred[0][1]))
    del game

    return score(count_of_ticks/500, food_gathered)


def make_new_population(pairs_matrix, models):
    weights = np.array([model.get_weights()[0] for model in models])
    weights = weights[pairs_matrix]
    res = []
    for i in range(weights.shape[0]):
        res += make_hybrids(weights[i])
    return res


def save_models(models, prefix):
    # Сохренение
    for k in range(len(models)):
        models[k].save_weights('{0}_{1}.h5'.format(prefix, k))


def load_models(models, prefix):
    for k in range(len(models)):
        models[k].load_weights('{0}_{1}.h5'.format(prefix, k))

shape = DaGame.STAR_COLLIDER_COUNT_OF_CLASSES * DaGame.STAR_COLLIDER_ENDINGS_COUNT + 2 # 2 - speed (x,y) TODO ошибка?
models = create_population(population_size)

for i in range(n_epochs):
    if i == 0:
        save_models(models, "first_iter")
    if i == n_epochs - 1:
        save_models(models, "last_iter")
    scores = estimate_scores(models, i)
    choices = np.random.choice(np.arange(population_size), p=softmax(scores), size=population_size)
    print(scores)

    new_population = make_new_population(choices.reshape(population_size // 2, 2), models)

    for model, matrix in zip(models, new_population):
        model.set_weights([matrix, model.get_weights()[1]])

