from pygame import *

from Entities.Player import *
from Entities.Enemy import *
from Entities.Food import *
from StarCollider import *

from Constants import *

import time
import random


class Game:
    def create_entities(self, n_enemies, n_food):
        # cоздаем объекты *************
        player1 = Player(center=[random.uniform(PLAYABLE_RECT[0], PLAYABLE_RECT[2]),
                                 random.uniform(PLAYABLE_RECT[1], PLAYABLE_RECT[3])],
                         speed=[0, 0])
        self.players.append(player1)
        self.star_collider = StarCollider(STAR_COLLIDER_RADIUS, STAR_COLLIDER_ENDINGS_COUNT, player1)

        for i in range(n_enemies):
            self.enemies.append(Enemy(center=[random.uniform(PLAYABLE_RECT[0], PLAYABLE_RECT[2]),
                                              random.uniform(PLAYABLE_RECT[1], PLAYABLE_RECT[3])],
                                      speed=[random.uniform(-ENEMY_MAX_SPEED, ENEMY_MAX_SPEED),
                                             random.uniform(-ENEMY_MAX_SPEED, ENEMY_MAX_SPEED)]))
        for i in range(n_food):
            self.food.append(Food(center=[random.uniform(PLAYABLE_RECT[0], PLAYABLE_RECT[2]),
                                          random.uniform(PLAYABLE_RECT[1], PLAYABLE_RECT[3])]))

        self.entities = []
        self.entities.append(self.star_collider)

        self.entities.extend(self.players)
        self.entities.extend(self.enemies)
        self.entities.extend(self.food)

    def __del__(self):
        if self.is_graphic:
            pygame.quit()

    def __init__(self, is_graphic, fixed_fps=60, seed=88005553535, debug=True):
        random.seed(a=seed, version=2)
        self.is_graphic = is_graphic
        self.fixed_fps = fixed_fps
        if is_graphic:
            pygame.init()                                        # Инициация PyGame, обязательная строчка
            self.screen = pygame.display.set_mode(DISPLAY)       # Создаем окошко
            pygame.display.set_caption("Do u no da way?")        # Пишем в шапку
            self.bg = Surface((WIN_WIDTH, WIN_HEIGHT))           # Создание видимой поверхности
        self.food = []
        self.players = []
        self.entities = []
        self.enemies = []
        self.star_collider = None
        self.fps_ticks = 0
        self.fps_start_time = time.time()
        self.debug = debug

        self.x = 0  # TODO Убрать
        self.y = 0

    def set_epoch(self, epoch):
        if self.is_graphic:
            pygame.display.set_caption("epoch: %d" % epoch)


    # Нужно бы позаботиться о нормализации
    def control(self, nplayer=0, direction=(0, 0)):
        self.players[nplayer].go(direction)

    def make_tick(self):
        self.fps_ticks += 1
        fps_last_tick_time = time.time()

        if self.is_graphic:
            for e in pygame.event.get():  # Обрабатываем события
                if e.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                # TODO Убрать Этого блока в будущем быть не должно :)
                if e.type == KEYDOWN and e.key == K_LEFT:
                    self.x = -1
                if e.type == KEYDOWN and e.key == K_RIGHT:
                    self.x = 1
                if e.type == KEYDOWN and e.key == K_UP:
                    self.y = -1
                if e.type == KEYDOWN and e.key == K_DOWN:
                    self.y = 1
                if e.type == KEYUP and e.key == K_LEFT:
                    self.x = 0
                if e.type == KEYUP and e.key == K_RIGHT:
                    self.x = 0
                if e.type == KEYUP and e.key == K_UP:
                    self.y = 0
                if e.type == KEYUP and e.key == K_DOWN:
                    self.y = 0
            self.control(0, (self.x, self.y))
            # TODO endof убрать
        # Обновление
        for ent in self.entities:
            ent.update(possible_rect=PLAYABLE_RECT)
            if not ent.isAlive:
                if type(ent) is Food:
                    self.food.remove(ent)    # TODO сделать обработку игроков
                self.entities.remove(ent)

        self.make_collider_tick()
        self.star_collider.find_collision(enemies_list=self.enemies, food_list=self.food)
        # endof Обновление
        # Отрисовка
        if self.is_graphic:
            self.bg.fill(COLOR_BG)
            for ent in self.entities:
                ent.draw(self.bg)
            self.screen.blit(self.bg, (0, 0))
            pygame.display.update()

        # ФПС
        if time.time() - self.fps_start_time > 1:
            if self.debug:
                print("FPS: ", self.fps_ticks / (time.time() - self.fps_start_time))  # FPS = 1 / time to process loop
                print("Score: ", self.players[0].get_score())
                print("foodsCount: ", len(self.food))
                print("entityCount: ", len(self.entities))
            self.fps_ticks = 0
            self.fps_start_time = time.time()

        # Беспощадный ограничитель кадров
        if self.is_graphic:
            while time.time() - fps_last_tick_time < 1.0 / self.fixed_fps:
                time.sleep(0.0001)

    def make_collider_tick(self):
        for player in self.players:
            for enemy in self.enemies:  # [x for x in self.enemies if x.is_collide(player)]:
                if player.is_collide(enemy):
                    player.die()
            for food in self.food:
                if player.is_collide(food):
                    food.die()
                    player.add_score()

    # Дает всю нужную инфу агенту
    def get_environment_info(self):
        info = []
        for player in self.players:
            player_info = [player.isAlive,
                           player.get_score(),
                           player.speed[0],
                           player.speed[1],
                           self.star_collider.collision]
            # player_info.extend(self.star_collider.collision)
            info += player_info
        return info

